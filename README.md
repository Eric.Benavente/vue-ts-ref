# vue-ts-ref
## This is an example on how to use refs with typescript and vue. 
see https://vuejs.org/v2/api/#ref


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your unit tests
```
yarn run test:unit
```
